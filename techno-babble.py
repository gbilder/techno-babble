import typer
from pathlib import Path
import random


verb = [
    "Initializing",
    "Booting",
    "Repairing",
    "Loading",
    "Checking",
    "Recalibrating",
    "Updating",
    "Installing",
    "Uninstalling",
    "Scanning",
    "Encrypting",
    "Decrypting",
    "Syncing",
    "Backing Up",
    "Restoring",
    "Rebooting",
    "Configuring",
    "Compiling",
    "Deploying",
    "Debugging",
    "Optimizing",
    "Logging",
    "Monitoring",
    "Analyzing",
    "Finalizing",
]
adjective = [
    "main",
    "radiant",
    "silent",
    "harmonic",
    "fast",
    "backup",
    "redundant",
    "multi-spectrum",
    "poly-phase",
    "vibrant",
    "dynamic",
    "seamless",
    "intuitive",
    "robust",
    "scalable",
    "innovative",
    "efficient",
    "versatile",
    "durable",
    "sleek",
    "immersive",
    "adaptive",
    "precise",
    "ambient",
    "encrypted",
]
noun = [
    "discharge array",
    "gravitational scrambler",
    "FTL (Faster Than Light) node",
    "greeble",
    "reflector",
    "interface coupler",
    "quantum resonator",
    "photon amplifier",
    "hyperdrive coil",
    "nano-fabricator",
    "ion disruptor",
    "plasma conduit",
    "warp stabilizer",
    "dimensional gateway",
    "antimatter injector",
    "singularity core",
    "holographic matrix",
    "temporal modulator",
    "subspace antenna",
    "energy lattice",
    "magnetic flux regulator",
    "neutron absorber",
    "vortex generator",
    "cryo chamber",
    "fusion reactor",
]


def technobabble() -> str:
    return f"{random.choice(verb)} {random.choice(adjective)} {random.choice(noun)}"


def main(lines: int = 10):
    typer.echo(f"Generating {lines} lines of technobabble")
    typer.echo("")

    for _ in range(lines):
        typer.echo(technobabble())


if __name__ == "__main__":
    typer.run(main)
